package tennis;

public class TennisGame1 implements TennisGame {

    private static final String SCORE_SEPARATOR = "-";
    private static final String SCORE_ALL = "All";
    private static final String SCORE_EQUALITY = "Deuce";
    private static final String SCORE_3 = "Forty";
    private static final String SCORE_2 = "Thirty";
    private static final String SCORE_1 = "Fifteen";
    private static final String SCORE_0 = "Love";
    private static final String WIN_FOR_PLAYER2 = "Win for player2";
    private static final String WIN_FOR_PLAYER1 = "Win for player1";
    private static final String ADVANTAGE_PLAYER2 = "Advantage player2";
    private static final String ADVANTAGE_PLAYER1 = "Advantage player1";
    private static final String PLAYER_1 = "player1";
    private Player player1;
    private Player player2;

    public TennisGame1(String player1Name, String player2Name) {
        this.player1 = new Player(player1Name);
        this.player2 = new Player(player2Name);
    }

    public void wonPoint(String playerName) {
        if (playerName == PLAYER_1) {
            player1.wonPoint();
        } else {
            player2.wonPoint();
        }
    }

    public String getScore() {
        String score = "";
        int tempScore = 0;
        if (scoreIsEqual()) {
            score = getEqualityPlayerScore(player1.getScore());
        } else if (scoreIsAdvantageOrWin()) {
            score = getAdvantageOrWinScore();
        } else {
            for (int i = 1; i < 3; i++) {
                if (i == 1) {
                    tempScore = player1.getScore();
                } else {
                    score += SCORE_SEPARATOR;
                    tempScore = player2.getScore();
                }
                score += getPlayerScore(tempScore);
            }
        }
        return score;
    }

    private boolean scoreIsAdvantageOrWin() {
        return player1.getScore() >= 4 || player2.getScore() >= 4;
    }

    private String getAdvantageOrWinScore() {
        int minusResult = player1.getScore() - player2.getScore();
        if (minusResult == 1) {
            return ADVANTAGE_PLAYER1;
        }
 
        if (minusResult == -1) {
            return ADVANTAGE_PLAYER2;
        }

        if (minusResult >= 2) {
            return WIN_FOR_PLAYER1;
        }

        return WIN_FOR_PLAYER2;
    }

    private boolean scoreIsEqual() {
        return player1.getScore() == player2.getScore();
    }

    private String getPlayerScore(int score) {
        switch (score) {
            case 0:
                return SCORE_0;
            case 1:
                return SCORE_1;
            case 2:
                return SCORE_2;
            case 3:
                return SCORE_3;
            default:
                return "";
        }
    }
    private String getEqualityPlayerScore(int score) {
        if (score >= 3) {
            return SCORE_EQUALITY;
        }
        return getPlayerScore(score) + SCORE_SEPARATOR + SCORE_ALL;
    }
}